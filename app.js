const { query } = require('express');
const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds 2 numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @return {Number}
 */
const add = (a,b) => {
    return a * b;
}

app.get('/', (req, res) => {
    const x = add(5,2);
    res.send(`Five multiplied by two is: ${x}`);
  })

/**app.get('/dadd/', (re, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    console.log({isANaN: isNaN(a), isBNaN(b)});
    if (isNaN(a) || isNaN(b)) {
        res.send(`params a: ${a}, b: ${b}`);
    } else {
        const sum = add(a,b);
        res.send(`Sum: ${sum.toString()}`);
    }
})*/

/**app.get('/', (req, res) => {
  res.send('Hello World!')
})*/

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})
