# **Markdown test page for Node Task**

Testing markdown for node task assignment

```
// testing codeblock
{
    "firstname": "John",
    "lastname": "Smith",
    "age": 25
}
```
---
## **Testing list**

1. First
2. Second
3. Third
4. Fourth
---
## **Testing terminal commands**

```sh
git clone https://gitlab.com/roni.niit/node-task
cd ./node-task
npm i
```

Start cmd: `npm run start`

---

## **Testing images**

![Text here](bonjourbear.jpg)

---

> *Testing blockquote and italic*
---

~~Testing strikethrough.~~

---
Testing Checklist

- [x] One
- [ ] Two
- [ ] Three 

---




